from pageserver.utils.struct import Wrapper
import time

class MemoryCache(object):
    """
    Redis Cache.
    """
    def __init__(self):
        self._data = {}
        self._expire = {}

    def delete(self, name):
        if name in self._data:
            del self._data[name]
        if name in self._expire:
            del self._expire[name]

    def set(self, name, value, timeout=60):
        self._data[name] = value
        if timeout is not None:
            self._expire[name] = int(time.time())+timeout

    def get(self, name):
        value = self._data.get(name)
        ex = self._expire.get(name)

        if ex is not None:
            now = int(time.time())
            if now > ex:
                self.delete(name)
                return None

        if value is None:
            return None

        return value

    def reset(self, name, value):
        self._data[name] = value

        if name in self._expire:
            ex = self._expire[name]
            now = int(time.time())
            if now > ex:
                self.delete(name)

    def ttl(self, name):
        return None

    def expire(self, name, seconds, alias="default"):
        return None

    def list_add(self, name, value):
        pass

    def list_rm(self, name, value):
        pass

    def list_get(self, name):
        pass

    def flag_items(self, name):
        pass

    def flag_add(self, name, key, value):
        pass

    def flag_set(self, name, key, value):
        pass

    def flag_get(self, name, key):
        pass

    def keys(self, cursor):
        pass

    def size(self, alias="size"):
        pass

    def get_type(self, key):
        pass
