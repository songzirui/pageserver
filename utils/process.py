"""
后台运行
"""

from multiprocessing import Process


def run(f, args):
    p = Process(target=f, args=args)
    p.start()
