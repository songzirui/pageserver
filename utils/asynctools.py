import asyncio


def async_to_sync(task):
    asyncio.run(task)


async def cancel_tasks(tasks):
    for task in tasks:
        task.cancel()
        try:
            await task
        except:
            pass

async def await_many_dispatch(consumer_callables, dispatch, tasks=None):
    """
    Given a set of consumer callables, awaits on them all and passes results
    from them to the dispatch awaitable as they come in.
    """
    # Start them all off as tasks
    loop = asyncio.get_event_loop()

    if tasks is None:
        tasks = []
    for consumer_callable in consumer_callables:
        tasks.append(loop.create_task(consumer_callable()))

    try:
        while True:
            # Wait for any of them to complete
            await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
            # Find the completed one(s), yield results, and replace them
            for i, task in enumerate(tasks):
                if task.done():
                    result = task.result()
                    await dispatch(result)
                    tasks[i] = asyncio.ensure_future(consumer_callables[i]())
    finally:
        # Make sure we clean up tasks on exit
        await cancel_tasks(tasks)
