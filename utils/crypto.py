"""
"""
import hashlib
from random import SystemRandom
import base64
import hmac
import time
_sysrand = SystemRandom()


def get_random_string(length=12,
                      allowed_chars='abcdefghijklmnopqrstuvwxyz'
                                    'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    """
    Return a securely generated random string.

    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits
    """
    return ''.join(_sysrand.choice(allowed_chars) for i in range(length))


def create_session_id():
    res = "{}{}".format(time.time_ns(), get_random_string(32))
    return res


def create_token(value, secret_key, expire=60*60*24*7, create_time=None):
    """
    :param value: str 加密消息
    :param secret_key: str 密钥
    :param expire: 过期时间，秒
    :param create_time 创建时间
    :return: sha1(40)+base64(expire_time(10)+message)
    """
    if create_time is None:
        create_time = time.time()
    message = "{}-{}-{}".format(int(create_time), expire, value)
    # print(message, message.split('-', 2))
    b64_message = base64.urlsafe_b64encode(message.encode('utf-8'))

    _token = hmac.new(secret_key.encode('utf-8'), b64_message, 'sha1').hexdigest()

    # print(_token, len(_token))
    token = "{}{}".format(_token, b64_message.decode('utf-8'))
    # print(token, len(token))
    return token


def check_token(token, key):
    """
    token 格式
    :param key:
    :param token:
    :return:
    """
    if not token:
        return False

    _token, b64_message = token[:40], token[40:]

    sha1 = hmac.new(key.encode("utf-8"), b64_message.encode('utf-8'), 'sha1').hexdigest()
    if sha1 != _token:
        return False

    message = base64.urlsafe_b64decode(b64_message).decode('utf-8').split('-', 2)
    create_time = float(message[0])
    expire_time = create_time+float(message[1])
    if expire_time < time.time():
        return False

    return message[2]


def make_password(password, salt=None, iterations=150000):
    if not salt:
        salt = get_random_string()
    hash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt.encode('utf-8'), iterations)
    hash = base64.b64encode(hash).decode('ascii').strip()
    return "%s$%d$%s$%s" % ("pbkdf2_sha256", iterations, salt, hash)


def check_password(password, encoded):
    algorithm, iterations, salt, hash = encoded.split('$', 3)
    encoded_2 = make_password(password, salt, int(iterations))
    return hmac.compare_digest(encoded.encode('utf-8'), encoded_2.encode('utf-8'))


def test():
    key = 'CC,$G!&Nj?pRjiL}WkNq#?!D^ikj.kASOED1*~PNjn,s%8u->hvZ2bfJEe-+(&n1'
    print(len(key))
    token = create_token("assssssssssssssss-oooooooooooooooooooooooooooooooe", secret_key=key)
    value = check_token(token, key=key)
    print(value)

    password = "abc"
    res = make_password(password)
    print(res)
    print(check_password(password, res))

if __name__ == '__main__':
    key = 'CC,$G!&Nj?pRjiL}WkNq#?!D^ikj.kASOED1*~PNjn,s%8u->hvZ2bfJEe-+(&n1'
    res = create_token("1234", key)
    print(res)
    res2 = check_token(res, key)
    print(res2)

