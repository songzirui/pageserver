import msgpack
from datetime import datetime, date
import copy


def msgpack_decode(obj):
    if '@datetime' in obj:
        obj = datetime.strptime(obj['_'], "%Y%m%dT%H:%M:%S.%f")
    elif '@date' in obj:
        obj = datetime.strptime(obj['_'], "%Y%m%d").date()
    return obj


def msgpack_encode(obj):
    if isinstance(obj, datetime):
        obj = {'@datetime': True, '_': obj.strftime("%Y%m%dT%H:%M:%S.%f")}
    elif isinstance(obj, date):
        obj = {'@date': True, '_': obj.strftime("%Y%m%d")}
    return obj


class Wrapper:

    @classmethod
    def serialize(cls, value):
        return msgpack.dumps(value, default=msgpack_encode)

    @classmethod
    def deserialize(cls, value):
        return msgpack.loads(value, object_hook=msgpack_decode)


class QueryDict(dict):

    def __getitem__(self, key):
        v = super().__getitem__(key)
        if isinstance(v, list):
            return v[-1]
        return v

    def get(self, key, default=None):
        try:
            val = self[key]
        except KeyError:
            return default
        if not val:
            return default
        return val

    def getlist(self, key, default=None):
        try:
            v = super().__getitem__(key)
            if isinstance(v, list):
                return v
            if not v:
                return []
            return [v, ]
        except KeyError:
            return []

    def copy(self):
        return copy.deepcopy(self)


if __name__ == '__main__':
    d = QueryDict({'a':[1,2,3,4], 'b': 1, 'c': { 'c1': 1}})
