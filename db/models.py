from pageserver.db.fields import *
from pageserver.db import database
from pageserver.db.query import Query


class ModelBase(type):

    def __new__(mcs, class_name, bases, attrs):
        meta = {
            'db_table': class_name.lower(),
            'verbose_name': class_name,
            'fields': {},
            'use_db': 'default',
            'primary_key': None,
        }
        _attrs = {}

        _meta = {}

        for key, val in attrs.items():
            if key == "meta":
                _meta = val
            elif isinstance(val, Field):
                meta["fields"][key] = val
                if val.attrs["primary_key"]:
                    meta["primary_key"] = key
            else:
                _attrs[key] = val

        meta.update(_meta)
        _attrs["_meta"] = meta
        return super().__new__(mcs, class_name, bases, _attrs)


class Model(metaclass=ModelBase):
    #
    # meta = {
    #    'db_table': 'device',
    #    'verbose_name': '设备',
    #    'unique_together': ""
    # }
    #
    @classmethod
    def load(cls, **kwargs):
        """数据库载入"""
        obj = cls()
        for k, v in kwargs.items():
            field = cls._meta['fields'].get(k)
            if v is not None:
                v = field.to_python(v)
            setattr(obj, k, v)

    def __init__(self, **kwargs):
        self.instance = None

        for k, field in self._meta['fields'].items():
            default = field.attrs["default"]
            v = default() if callable(default) else default
            setattr(self, k, v)

        for k, v in kwargs.items():
            setattr(self, k, v)

    def to_dict(self, fields="all"):
        res = {}
        if fields == "all":
            fields = self._meta['fields']

        for k in fields:
            res[k] = getattr(self, k)
        return res

    @classmethod
    def query(cls, *args, **kwargs):
        use_db = cls._meta['use_db']
        query = Query(db=database[use_db], model_class=cls).query(*args, **kwargs)
        return query

    def pre_save(self):
        # 验证数据
        for name, field in self._meta['fields'].items():
            value = field.to_database(getattr(self, name))
            # 空
            if value is None and not field.attrs['nullable']:
                raise Exception("{} not nullable".format(name))

            setattr(self, name, value)

            if 'choice' in field.attrs:
                if value not in [o[0] for o in field.attrs['choice']]:
                    raise Exception("{} value not in choice".format(name))

    def post_save(self):
        pass

    def save(self, use_db=None, conn=None, fields="__all__"):
        """
        fields = update 有效
        """
        self.pre_save()
        if use_db is None:
            use_db = self._meta['use_db']
        if self.instance is None:
            database[use_db].insert(self, conn)
        else:
            database[use_db].update(self, conn, fields)

        try:
            self.post_save()
        except:
            pass
        return self

    def delete(self, use_db="default", conn=None):
        if self.instance is not None:
            primary_key = self._meta['primary_key']
            self.query(**{primary_key: self.instance}).use_conn(conn).delete()
            self.instance = None
