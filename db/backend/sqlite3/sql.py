from pageserver.db.fields import *
__all__ = ["SQL"]


def CONSTRAINT(field):
    res = ""
    if not field.attrs["nullable"]:
        res += " NOT NULL"
    if field.attrs["unique"]:
        res += " UNIQUE"

    if field.attrs["primary_key"]:
        res += " PRIMARY KEY"

    return res

def DEFAULT(field, wrapper=''):
    default_value = field.attrs["default"]
    if default_value is not None:
        if not callable(default_value):
            return " DEFAULT {}{}{}".format(wrapper, field.attrs["default"], wrapper)
    return ''


def TEXT(field):
    max_length = field.attrs['max_length']
    res = "TEXT({})".format(max_length) if max_length else "TEXT"
    return res+CONSTRAINT(field)


def INT(field):
    res = "INTEGER"

    res += DEFAULT(field)
    res += CONSTRAINT(field)
    if field.attrs.get('auto_increment'):
        res += " AUTOINCREMENT"

    return res


def REAL(field):
    res = "REAL"

    if field.attrs['default']:
        res += " DEFAULT {}".format(field.attrs['default'])
    return res+CONSTRAINT(field)


MAPPING = [
    [TextField, TEXT],
    [CharField, TEXT],
    [IntegerField, INT],
    [BigIntegerField, INT],
    [FloatField, REAL],
    [DoubleField, REAL],
]


class SQL:
    @classmethod
    def field(cls, field):
        for f in MAPPING:
            if isinstance(field, f[0]):
                return f[1](field)

        raise Exception("unknown field {}".format(field))
