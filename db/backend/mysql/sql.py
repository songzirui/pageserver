from pageserver.db.fields import *
__all__ = ["SQL"]


def CONSTRAINT(field):
    res = ""
    if not field.attrs["nullable"]:
        res += " NOT NULL"
    if field.attrs["unique"]:
        res += " UNIQUE"

    if field.attrs["primary_key"]:
        res += " PRIMARY KEY"

    return res


def DEFAULT(field, wrapper=''):
    default_value = field.attrs["default"]
    if default_value is not None:
        if not callable(default_value):
            return " DEFAULT {}{}{}".format(wrapper, field.attrs["default"], wrapper)
    return ''


def VARCHAR(field):
    max_length = field.attrs['max_length']
    res = "VARCHAR({})".format(max_length)
    res += DEFAULT(field, "'")
    return res+CONSTRAINT(field)

def TEXT(field):
    max_length = field.attrs['max_length']
    res = "TEXT({})".format(max_length) if max_length else "TEXT"
    return res+CONSTRAINT(field)


def INT(field):
    res = "INT"
    if field.attrs.get('auto_increment'):
        res += " AUTO_INCREMENT"

    res += DEFAULT(field)
    return res+CONSTRAINT(field)


def BIGINT(field):
    res = "BIGINT"
    if field.attrs.get('auto_increment'):
        res += " AUTO_INCREMENT"

    res += DEFAULT(field)
    return res+CONSTRAINT(field)


def FLOAT(field):
    res = "FLOAT"
    res += DEFAULT(field)
    return res+CONSTRAINT(field)


def DATE(field):
    res = "DATE"
    res += DEFAULT(field)
    return res+CONSTRAINT(field)


MAPPING = [
    [TextField, TEXT],
    [CharField, VARCHAR],
    [BigIntegerField, BIGINT],
    [IntegerField, INT],
    [FloatField, FLOAT],
    [DateField, DATE],
]



class SQL:
    @classmethod
    def field(cls, field):
        for f in MAPPING:
            if isinstance(field, f[0]):
                return f[1](field)

        raise Exception("unknown field {}".format(field))
