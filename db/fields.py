import types
import os
import json


class Field(object):
    field_type = "field"

    def __init__(self, nullable=True, default=None, primary_key=False, unique=False, **attrs):
        attrs.update({
            "default": default,
            "nullable": nullable,
            "unique": unique,
            "primary_key": primary_key,
            "type": self.field_type,
        })
        self.attrs = attrs

    def to_database(self, val):
        """保存到数据库时调用，一般转换成字符串,或数字"""
        return val

    def to_python(self, val):
        """转换成python对象时调用"""
        return val


class CharField(Field):
    field_type = "char"

    def __init__(self, max_length, default="", **kwargs):
        super().__init__(max_length=max_length, default=default, **kwargs)

    def to_database(self, val):
        return str(val)

    def to_python(self, val):
        return str(val)


class TextField(Field):
    field_type = "text"

    def __init__(self, max_length=None, default="", **kwargs):
        super().__init__(max_length=max_length, default=default, **kwargs)

    def to_database(self, val):
        return str(val)

    def to_python(self, val):
        return str(val)


class JsonField(TextField):

    def __init__(self, default=None, **kwargs):
        if 'max_length' in kwargs:
            del kwargs['max_length']
        super().__init__(max_length=None, default=default, **kwargs)

    def to_database(self, val):
        if val in ['', None]:
            return val

        if isinstance(val, str):
            val = json.loads(val)
        try:
            val = json.dumps(val)
        except:
            raise Exception(f'the value({val}) can\'t dumps with json')
        return val

    def to_python(self, val):
        if val in ['', None]:
            return val

        if isinstance(val, str):
            return json.loads(val)
        return val


class FileField(TextField):
    field_type = "file"

    def __init__(self, upload_to="", **kwargs):
        kwargs.update({
            'upload_to': upload_to,
            'max_length': None,
            'default': '',
        })
        super().__init__(**kwargs)

    def to_database(self, val):
        if not isinstance(val, str):
            from pageserver.conf import settings
            import time
            save_dir = os.path.join(settings.MEDIA_DIR, self.attrs['upload_to'])
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)

            _filename = getattr(val, "filename")
            filename = _filename or str(time.time())
            save_value = "/".join([self.attrs['upload_to'], filename])
            save_as = os.path.join(save_dir, filename)

            i = 0
            while True:
                if not os.path.exists(save_as):
                    break

                if i > 10:
                    raise Exception("filename errors")

                filename = "{}_{}".format(str(time.time()), _filename) if _filename else str(time.time())
                save_value = "/".join([self.attrs['upload_to'], filename])
                save_as = os.path.join(save_dir, filename)
                i += 1

            with open(save_as, 'wb') as wf:
                wf.write(val.read())
            return save_value

        return val


class ImageField(FileField):
    field_type = "image"


class IntegerField(Field):
    field_type = "int"

    def to_database(self, val):
        if val in [None, '', 'null', 'undefined', 'None']:
            return None
        return int(val)

    def to_python(self, val):
        return int(val)


class BigIntegerField(IntegerField):
    field_type = "char"


class TimestampField(IntegerField):
    field_type = "timestamp"


class BigTimestampField(BigIntegerField):
    field_type = "timestamp"


class DateField(Field):
    field_type = "date"

    def to_database(self, val):
        return str(val)

    def to_python(self, val):
        return str(val)


class AutoField(IntegerField):
    field_type = "auto"

    def __init__(self, **kwargs):
        kwargs.update({
            "auto_increment": True,
            "primary_key": True,
        })
        super().__init__(**kwargs)


class BigAutoField(BigIntegerField):

    field_type = "auto"

    def __init__(self, **kwargs):
        kwargs.update({
            "auto_increment": True,
            "primary_key": True,
        })
        super().__init__(**kwargs)


class FloatField(Field):
    field_type = "float"

    def to_database(self, val):
        return float(val)

    def to_python(self, val):
        return float(val)


class DoubleField(FloatField):
    field_type = "float"
