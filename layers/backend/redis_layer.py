import time
import asyncio
import aioredis
import msgpack


class Wrapper:
    @classmethod
    def get_channel_name(cls, channel):
        return "channel.{}".format(channel)

    @classmethod
    def get_group_name(cls, group):
        return "group.{}".format(group)

    @classmethod
    def serialize(cls, text_data=None, bytes_data=None):
         return msgpack.dumps({
             'type': 'channel', 'text': text_data, 'bytes': bytes_data
         })

    @classmethod
    def deserialize(cls, message):
        return msgpack.loads(message)


class RedisLayer(object):
    """
    Redis channel layer.

    It routes all messages into remote Redis server. Support for
    sharding among different Redis installations and message
    encryption are provided.
    """
    def __init__(self):
        self.capacity = 1024
        self.expiry = 3600
        self.redis = None
        self.host = None
        self.db = 0

    async def setup(self, host, port, db=0, **kwargs):
        self.host = (host, port)
        self.db = db
        self.redis = await aioredis.create_redis_pool(self.host, db=db)


    async def send(self, channel, text_data=None, bytes_data=None):
        """
        Send a message onto a (general or specific) channel.
        """
        channel = Wrapper.get_channel_name(channel)
        await self.redis.rpush(channel, Wrapper.serialize(text_data, bytes_data))

    async def receive(self, channel):
        def finished(*args, **kwargs):
            conn.close()

        channel = Wrapper.get_channel_name(channel)
        conn = await aioredis.create_redis(self.host, db=self.db)
        task_wait = conn.blpop(channel)
        task_wait.add_done_callback(finished)
        _, data = await task_wait
        data = Wrapper.deserialize(data)
        return data

    async def unbind(self, channel):
        """解绑 receive"""
        channel = Wrapper.get_channel_name(channel)
        await self.redis.delete(channel)

    async def clean(self):
        """解绑 receive"""
        await self.redis.execute('flushdb')

    async def group_join(self, group, channel):
        """
        Adds the channel name to a group.
        """
        group = Wrapper.get_group_name(group)
        self.redis.zadd(group, time.time(), channel)

    async def group_discard(self, group, channel):
        group = Wrapper.get_group_name(group)
        await self.redis.zrem(group, channel)

    async def group_members(self, group):
        """获取 group 中的成员"""
        return [x.decode('utf-8') for x in await self.redis.execute('zrange', group, 0, -1)]

    async def group_send(self, group, text_data=None, bytes_data=None):
        """
        Sends a message to the entire group.
        """
        group = Wrapper.get_group_name(group)
        for channel in await self.group_members(group):
            await self.send(channel, text_data, bytes_data)






