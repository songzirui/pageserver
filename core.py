import importlib
from .router import UrlRouter
from .conf import settings
from .http.response import Http404


class PageApp(object):
    middlewares = []

    def load_middleware(self, middleware_list):
        """加载中间件"""
        for path in middleware_list:
            mods = path.split(".")
            package_name = ".".join(mods[:-1])
            package = importlib.import_module(package_name)
            self.middlewares.append(getattr(package, mods[-1])())

    def __init__(self, custom_settings=None):
        self.settings = custom_settings
        self.router = None

    async def setup(self):
        await settings.setup(self.settings)
        self.load_middleware(settings.MIDDLEWARE)
        urls = importlib.import_module(settings.URLS)
        self.router = UrlRouter(getattr(urls, 'urlpatterns', []))

    def middleware_process(self, request):
        for handle in self.middlewares:
            response = handle.request_handle(request)
            if response:
                return response

    def http_process(self, request):
        view, kwargs = self.router.get_url_view(request.path)
        if callable(view):
            response = self.middleware_process(request)
            if response:
                return response
            return view(request, **kwargs)

        return Http404()

    async def ws_process(self, request, reader, writer):
        try:
            view, kwargs = self.router.get_url_view(request.path)
            if callable(view):
                await view(request, reader, writer, **kwargs)
        except ConnectionResetError:
            pass
