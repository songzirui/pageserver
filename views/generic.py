from pageserver.conf import settings
from pageserver.views.base import View, API, CorsMixin
from pageserver.db.query import Q
from pageserver.http.response import HttpResponse, JsonResponse
from pageserver.http.exception import Error
from pageserver.db.fields import *
from pageserver.utils import log
import copy

import os

class TemplateView(View):
    _template = None
    use_cache = False
    template_name = None

    def get_template_name(self):
        return self.template_name

    def get_template(self):
        cls = self.__class__
        if cls._template:
            return cls._template

        path = os.path.join(settings.TEMPLATE_DIR, self.get_template_name())
        with open(path, 'rb') as rf:
            html = rf.read()

        if self.use_cache:
            cls._template = html

        return html

    def get(self, request, **kwargs):
        return HttpResponse(body=self.get_template())


class PermAPI(API):
    def check_perm(self):
        return True

    @classmethod
    def as_view(cls, request, **kwargs):
        view = cls(request)
        http_method = request.method.lower()
        try:
            if http_method != "options" and not bool(view.check_perm()):
                return JsonResponse({'status': 'FAIL', 'errors': 'invalid perm'})

            if not hasattr(view, http_method):
                return JsonResponse({'status': 'FAIL', 'errors': 'invalid method'})

            response = getattr(view, http_method)(request, **kwargs)
            if isinstance(response, dict):
                response = JsonResponse(response)
            return response
        except Error as e:
            return JsonResponse({'status': 'FAIL', 'errors': str(e)})
        except Exception as e:
            if settings.DEBUG:
                import traceback
                traceback.print_exc()

            return JsonResponse({'status': 'FAIL', 'errors': '参数错误'})


class PermCorsMixin(object):
    cors_origin = "*"
    cors_header = "Content-Type"

    def options(self, request):
        response = HttpResponse()
        response.header["Access-Control-Allow-Origin"] = self.cors_origin
        response.header["Access-Control-Allow-Headers"] = request.META['access-control-request-headers']
        return response


    @classmethod
    async def as_view(cls, request, **kwargs):
        view = cls(request)
        http_method = request.method.lower()
        if http_method == "options":
            return view.options(request)

        try:
            if not hasattr(view, http_method):
                response = {'status': 'FAIL', 'errors': 'invalid method'}
            elif not bool(view.check_perm()):
                response = {'status': 'FAIL', 'errors': 'invalid perm'}
            else:
                response = getattr(view, http_method)(request, **kwargs)
                if isinstance(response, types.CoroutineType):
                    response = await response

            if isinstance(response, dict):
                response = JsonResponse(response)

        except Error as e:
            response = JsonResponse({'status': 'FAIL', 'errors': str(e)})

        except Exception as e:
            response = JsonResponse({'status': 'FAIL', 'errors': '参数错误'})
            if settings.DEBUG:
                import traceback
                traceback.print_exc()

        response.header['Access-Control-Allow-Origin'] = cls.cors_origin
        return response


class PermAdminAPI(PermAPI):
    model_class = None
    fields = "__all__"
    with_meta = True

    def check_perm(self):
        return False

    def get_filters(self):
        return {}

    def get_model_class(self):
        return self.model_class

    def _save_file(self, file_name, field_name, field):
        if field_name not in self.request.FILES:
            return file_name

        rf = self.request.FILES[field_name][0]
        save_dir = os.path.join(settings.MEDIA_DIR, field.attrs['upload_to'])
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        save_value = "/".join([field.attrs['upload_to'], file_name])
        save_as = os.path.join(save_dir, file_name)
        with open(save_as, 'wb') as wf:
            wf.write(rf.read())
            rf.close()

        return save_value


    def get_form_data(self, model_class):
        res = {}
        for field_name, field in model_class._meta['fields'].items():

            if field_name not in self.request.POST:
                continue

            value = self.request.POST[field_name]

            if isinstance(field, (IntegerField, FloatField)):
                if value in ['null', '']:
                    value = None

            if value is None:
                if not field.attrs['nullable']:
                    raise Exception("{} not nullable".format(field_name))
            else:
                value = field.to_python(value)

            res[field_name] = value

        return res

    def get(self, request):
        model_class = self.get_model_class()

        try:
            skip, limit = request.GET.get('skip', 0), request.GET.get('limit', 10)
            skip, limit = int(skip), int(limit)
            if limit > 100:
                limit = 100

            query = model_class.query(**self.get_filters())
            object_list = query.skip(skip).limit(limit).all(to='dict')
            res = {'status': 'OK', 'object_list': object_list}

            if skip == 0:
                if self.with_meta:
                    fields = []
                    for name in model_class._meta['fields']:
                        field = model_class._meta['fields'][name]
                        attrs = {}
                        for _k, _v in field.attrs.items():
                            if callable(_v):
                                _v = _v()
                            attrs[_k] = _v
                        attrs['name'] = name
                        fields.append(attrs)

                    res['fields'] = fields
                res['count'] = query.count()

            return res

        except Exception as e:
            log.error(e)
            return {'status': 'FAIL', 'errors': str(e)}


    def post(self, request):
        try:
            model_class = self.get_model_class()
            form_data = self.get_form_data(model_class)
            primary_key = model_class._meta['primary_key']
            pk = form_data.get(primary_key)
            action = request.GET['action']

            if action == 'edit':
                obj = model_class.query(**{primary_key: pk}).limit(1).one()
            elif action == 'new':
                obj = model_class()
            else:
                return {'status': 'FAIL', 'errors': 'unknown action'}

            for k, f in model_class._meta['fields'].items():
                if k in form_data:
                    setattr(obj, k, form_data[k])

                    if isinstance(f, FileField):

                        setattr(obj, k, self._save_file(form_data[k], k, f))

            # raise Exception('eee')
            obj.save()
            return {'status': 'OK', 'object': getattr(obj, primary_key)}
        except Exception as e:
            log.error(e)
            return {'status': 'FAIL', 'errors': str(e)}

    def delete(self, request):
        model_class = self.get_model_class()
        primary_key = model_class._meta['primary_key']
        pk = request.GET.get('pk')
        try:
            obj = model_class.query(**{primary_key: pk}).limit(1).one()
            obj.delete()
        except Exception as e:
            print(e)
            return {'status': 'FAIL', 'errors': '无法删除'}
        return {'status': 'OK'}


class PermListAPI(PermAPI):
    model_class = None
    fields = "__all__"
    exclude = []  # 排除的字段
    limit = 100
    filters = []
    order_by = []
    joins = []
    data_format = "dict"  # dict|list

    def check_perm(self):
        return False

    def get_filters(self):
        res = {}
        for k in self.filters:
            if k in self.request.GET:
                res[k] = self.request.GET[k]

        return res

    def get_skip_limit(self):
        skip = abs(int(self.request.GET.get('skip', 0)))
        limit = self.request.GET.get('limit', 10)
        limit = self.limit if limit == 'all' else abs(int(limit))

        if isinstance(self.limit, int):
            limit = min(limit, self.limit)
        return skip, limit

    def get_ordering(self):
        res = []
        if 'order_by' in self.request.GET:
            for v in self.request.GET['order_by'].split(','):
                if v in self.order_by:
                    res.append(v)

        return res

    def get_fields(self):
        all_fields = tuple(self.model_class._meta['fields'])

        if self.fields == '__all__':
            res = copy.copy(all_fields)
        else:
            res = copy.copy(self.fields)

        return res

    def get_joins(self):
        return self.joins

    def get_object_list(self, queryset):
        return queryset.all(to=self.data_format)

    def get_queryset(self):
        skip, limit = self.get_skip_limit()

        q = self.get_filters()
        if isinstance(q, dict):
            q = Q(**q)

        queryset = self.model_class.query(q) \
            .select(self.get_fields()) \
            .skip(skip).limit(limit) \
            .ordering(*self.get_ordering())

        for j in self.get_joins():
            queryset.join(**j)

        return queryset

    def get(self, request):
        try:
            queryset = self.get_queryset()
            dataset = self.get_object_list(queryset)

            res = {'status': 'OK', 'object_list': dataset}

            if hasattr(self, 'get_extra'):
                res['extra'] = getattr(self, 'get_extra')()

            if queryset._offset == 0:
                if self.data_format == 'list':
                    res['columns'] = queryset.columns(split="$")

                if queryset._limit:
                    res['count'] = queryset.count()

            return res

        except Exception as e:
            if settings.DEBUG:
                import traceback
                traceback.print_exc()
            return JsonResponse({'status': 'FAIL', 'errors': str(e)})


__all__ = [
    "TemplateView",
    "PermAPI",
    "PermAdminAPI",
    "API",
    "CorsMixin",
    "PermListAPI",
    "PermCorsMixin",
]
