from pageserver.http.response import Http405
from pageserver.http.response import JsonResponse, HttpResponse
from pageserver.http.websocket import WebSocket, OPCODE, WebSocketCloseException
from pageserver.utils.asynctools import await_many_dispatch, cancel_tasks
from pageserver.layers import layer
import functools
import types
import uuid
import asyncio


class View(object):
    _use_thread = False  # 使用新进程运行

    def __init__(self, request):
        self.request = request

    @classmethod
    def as_view(cls, request, **kwargs):
        view = cls(request)
        http_method = request.method.lower()
        if not hasattr(view, http_method):
            return Http405()
        return getattr(view, http_method)(request, **kwargs)


class API(View):
    @classmethod
    async def as_view(cls, request, **kwargs):
        view = cls(request)
        http_method = request.method.lower()
        if not hasattr(view, http_method):
            return Http405()

        response = getattr(view, http_method)(request, **kwargs)
        if isinstance(response, types.CoroutineType):
            response = await response

        if isinstance(response, dict):
            response = JsonResponse(response)

        return response


class CorsMixin(object):
    cors_origin = "*"

    def options(self, request):
        response = HttpResponse()
        response.header["Access-Control-Allow-Headers"] = request.META['access-control-request-headers']
        return response


    @classmethod
    async def as_view(cls, request, **kwargs):
        response = super().as_view(request, **kwargs)
        if isinstance(response, types.CoroutineType):
            response = await response

        response.header['Access-Control-Allow-Origin'] = cls.cors_origin
        return response


class Consumer(object):
    layer_alias = "default"

    """handshake->onopen->onmessage->onclose"""
    def __init__(self, request, reader, writer, **kwargs):
        self.request = request
        self.reader = reader
        self.writer = writer
        self.channel = uuid.uuid4().hex
        self.layer = layer.get(self.layer_alias)
        self.tasks = []

    def handshake(self):
        return True

    async def onopen(self):
        pass

    async def onmessage(self, text_data=None, bytes_data=None):
        pass

    async def onchannel(self, text_data=None, bytes_data=None):
        pass

    async def onclose(self):
        pass

    async def group_members(self, name):
        await self.layer.group_members(name)

    async def group_join(self, name):
        await self.layer.group_join(name, self.channel)

    async def group_send(self, name, text_data=None, bytes_data=None):
        await self.layer.group_send(name, text_data, bytes_data)

    async def group_discard(self, group_name):
        """离开group"""
        await self.layer.group_discard(group_name, self.channel)

    async def receive(self, message):
        if message['type'] == "ping":
            await WebSocket.send(self.writer, OPCODE.PONG)
            return

        if message['type'] == "websocket":
            await self.onmessage(message['text'], message['bytes'])
            return

        if message['type'] == "channel":
            await self.onchannel(message['text'], message['bytes'])
            return

    async def send(self, text_data=None, bytes_data=None):
        if text_data is not None:
            await WebSocket.send(self.writer, OPCODE.TEXT, text_data)
        if bytes_data is not None:
            await WebSocket.send(self.writer, OPCODE.BYTES, bytes_data)

    async def close(self):
        await WebSocket.close(self.writer)

    async def wait(self):

        try:
            await self.onopen()
            receive = functools.partial(WebSocket.receive, self.reader)
            fns = [receive, ]
            if self.layer:
                layer_receive = functools.partial(self.layer.receive, self.channel)
                fns.append(layer_receive)

            await await_many_dispatch(fns, self.receive, self.tasks)
        except WebSocketCloseException:
            pass
        except asyncio.exceptions.IncompleteReadError:
            pass
        except ConnectionResetError:
            pass
        finally:
            await self.onclose()
            await cancel_tasks(self.tasks)
            await WebSocket.disconnect(self.writer)
            await self.layer.unbind(self.channel)

    @classmethod
    async def as_view(cls, request, reader, writer, **kwargs):
        self = cls(request, reader, writer, **kwargs)
        if self.handshake():
            await WebSocket.handshake(self.request, self.writer)
            await self.wait()
        else:
            await self.close()


__all__ = [
    "View",
    "API",
    "Consumer",
]
