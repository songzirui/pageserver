from pageserver.views.generic import *
from pageserver.http.exception import Error
from pageserver.utils.crypto import create_token
from . import *
import json
import re


class LoginAPI(API):

    def get(self, request):
        if check_perm(request):
            return {'status': 'OK'}
        return {'status': 'FAIL'}

    def post(self, request):
        try:
            username = request.POST['username']
            password = request.POST['password']
            if not re.match(r"^[\w\d]{4,32}$", username):
                raise Exception()

            obj = AdminAccount.query(username=username).one()
            if obj and obj.check_password(password):
                val = json.dumps({
                    'id': obj.id,
                    '_': obj.token,
                })
                token = create_token(val, secret_key=settings.SECRET_KEY, expire=604800)
                return {
                    'status': 'OK',
                    'token': {
                        'key': 'aid',
                        'token': token,
                    }
                }
        except Error as e:
            return {'status': 'FAIL', 'errors': str(e)}
        except:
            return {'status': 'FAIL'}


class LoginProfileAPI(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def get(self, request):
        return {
            'status': 'OK',
            'profile': self.request.admin.to_dict(fields=["username", "nickname", "create_time", "token"])
        }


class UserListAPI(PermListAPI):
    model_class = AdminAccount
    fields = "__all__"
    exclude = ["password", "keygen"]

    def check_perm(self):
        return check_perm(self.request)


class UserResetPasswordAPI(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def post(self, request):
        pass
