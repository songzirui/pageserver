from pageserver.http.response import JsonResponse
from pageserver.views.generic import *
from pageserver.http.exception import Error
from pageserver.utils.crypto import create_token, check_token
from pageserver.cache import cache
from pageserver.conf import settings
from .action import check_perm
import re


class CacheDBAPI(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def get(self, request):
        return {'status': 'OK', 'result': list(settings.CACHE.keys())}


class CacheKeysAPI(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def get(self, request):
        alias = request.GET.get("db", "default")
        skip = int(request.GET.get('cursor', 0))
        cursor, data = cache.keys(cursor=skip, alias=alias)
        result = [x.decode('utf-8') for x in data]
        res = {'status': 'OK', 'keys': result, 'cursor': cursor}
        if cursor == 0:
            res['size'] = cache.size(alias=alias)

        return res


class CacheValueAPI(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def get(self, request):
        alias = request.GET["db"]
        k = request.GET['key']
        _t = cache.get_type(k, alias=alias).decode('utf-8')
        value = "-"
        if _t == 'string':
            value = cache.get(k, alias=alias)
        if _t == 'list':
            value = cache.list_get(k, alias=alias)

        return {'status': 'OK', 'value': value, 'type': _t, 'ttl': cache.ttl(k, alias=alias)}
