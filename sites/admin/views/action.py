from pageserver.views.generic import *
from pageserver.http.exception import Error
from pageserver.utils.crypto import create_token
from . import *
import json
import re
config = __import__(settings.ADMIN['config'])

class ModelView(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def get(self, request):
        models = []
        for model in config.MODEL_LIST:
            models.append({
                'verbose_name': model._meta['verbose_name'],
                'model_name': model.__name__,
            })
        return {'status': 'OK', 'model_list': models}


class AdminView(PermAdminAPI):
    def check_perm(self):
        return check_perm(self.request)

    def get_model_class(self):
        try:
            for model_class in config.MODEL_LIST:
                if model_class.__name__ == self.request.GET['model']:
                    return model_class

        except:
            return None


class DeleteObjsAPI(PermAPI):

    def check_perm(self):
        return check_perm(self.request)


    def get_model_class(self):
        try:
            for model_class in config.MODEL_LIST:
                if model_class.__name__ == self.request.GET['model']:
                    return model_class
        except:
            return None

    def post(self, request):
        try:
            model_class = self.get_model_class()
            key = model_class._meta['primary_key']
            pks = [int(x) for x in request.POST.getlist('pks')]
            model_class.query(**{f"{key}__in": pks}).delete()
            return {'status': 'OK'}
        except Exception as e:
            print(e)
            return {'status': 'FAIL', 'errors': '无法删除'}


class UserListAPI(PermListAPI):
    model_class = AdminAccount
    fields = "__all__"
    exclude = ["password", "keygen"]

    def check_perm(self):
        return check_perm(self.request)


class UserResetPasswordAPI(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def post(self, request):
        pass
