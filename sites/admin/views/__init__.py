from pageserver.views.generic import TemplateView
from pageserver.utils.crypto import check_token
from pageserver.conf import settings
from pageserver.sites.admin.model import AdminAccount
import json
import os


def check_perm(request):
    request.admin = None
    data = check_token(request.META.get('aid'), key=settings.SECRET_KEY)
    if data:
        data = json.loads(data)
        obj = AdminAccount.query(id=data['id']).one()
        if obj and obj.is_active and data.get('_') == obj.token:
            request.admin = obj
            return True


class IndexView(TemplateView):
    use_cache = not settings.DEBUG

    def get_template(self):
        if self.__class__._template:
            return self.__class__._template

        basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        path = os.path.join(basedir, 'templates', 'admin.html')
        # not cache
        with open(path, 'rb') as rf:
            html = rf.read()

        if self.use_cache:
            self.__class__._template = html

        return html
