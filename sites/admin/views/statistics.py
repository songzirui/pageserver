from pageserver.http.response import JsonResponse
from pageserver.views.generic import *
from pageserver.http.exception import Error
from pageserver.utils.crypto import create_token, check_token
from pageserver.cache import *
from pageserver.conf import settings
from .action import check_perm
import re


class Overview(PermAPI):
    def check_perm(self):
        return check_perm(self.request)

    def get(self, request):
        data = {
            'TOTAL_CONNECT': GLOBAL.TOTAL_CONNECT,
            'TOTAL_REQUEST': GLOBAL.TOTAL_REQUEST,
            'BAD_REQUEST': GLOBAL.BAD_REQUEST,
            'VALID_REQUEST': GLOBAL.VALID_REQUEST,
            'STATIC_REQUEST': GLOBAL.STATIC_REQUEST,
            'CURRENT_CONNECT': GLOBAL.CURRENT_CONNECT,
        }
        return {'status': 'OK', 'data': data}
