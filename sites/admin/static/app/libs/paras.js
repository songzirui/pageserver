var Host = {
   request(method, url, params, data, callback, context, headers){
      if(headers==undefined){
         headers = {}
      }
      try {
         let login = System.login.get();
         if(login){
            headers[login.key] = login.token;
         }
         axios({
             baseURL: System.host.get(),
             method: method,
             params: params,
             data: data,
             url: url,
             responseType: 'json',
             headers: headers,
         }).then(function(response){
            try{
               if(response.data.status=='AUTH'){
                  $toast("登录信息已变更，3秒后重新登录")
                  System.login.set(null)
                  setTimeout(function(){
                     location.reload()
                  }, 3000);
                  return
               }
            }catch(e){
               console.error(e)
            }
            
            callback.call(context, response.data)
         }).catch(function(e){
            console.log('request', url, e)
            callback.call(context, { 'status': 'FAIL', errors: '请求错误' + e })
         })
      } catch (e) {
         //console.log('request', url, e)
         callback.call(context, { 'status': 'FAIL', errors: '请求错误' + e })
      }

   },
   get(url, filter, callback, context) {
      switch (arguments.length) {
         case 4:
            break;
         case 3:
            if (typeof arguments[1] == 'function') {
               context = arguments[2];
               callback = arguments[1];
               filter = undefined;
            }
            break;
         case 2:
            callback = arguments[1];
            filter = undefined;
            context = undefined;
            break;

         default:
            console.error('Host.get need arguments >= 2')
            break
      }
      Host.request('get', url, filter, null, callback, context)

   },
   post(url, data, callback, context) {
      switch (arguments.length) {
         case 4:
         case 3:
            break;
         default:
            console.error('Host.post need arguments >= 3')
            return
      }
      let headers = {}
      if(data instanceof FormData){
         headers = {'Content-Type': 'multipart/form-data'}
      }
      Host.request('post', url, null, data, callback, context)
   },
   put(url, data, callback, context) {
      switch (arguments.length) {
         case 4:
         case 3:
            break;
         default:
            console.error('Host.put need arguments >= 3')
            return
      }
      Host.request('put', url, null, data, callback, context)
   },
   delete(url, data, callback, context) {
      switch (arguments.length) {
         case 4:
         case 3:
            break;
         default:
            console.error('Host.delete need arguments >= 3')
            return
      }
      Host.request('delete', url, data, null, callback, context)      
   },
}

var HostX = {
   async post(url, data) {
      return new Promise((resolve, reject) => {
         let xhr = new XMLHttpRequest();
         xhr.open('POST', url);
         if (!(data instanceof FormData)) {
            xhr.setRequestHeader('content-type', 'application/json');
            data = JSON.stringify(data)
         }
         let login = System.login.get();
         if (login) {
            xhr.setRequestHeader(login.key, login.token);
         }
         xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
               if (xhr.status == 200 || xhr.status == 304) {
                  try {
                     resolve(JSON.parse(xhr.responseText));
                  } catch (e) {
                     resolve(xhr.responseText)
                  }
               } else {
                  resolve({ 'status': 'FAIL', 'errors': xhr.status })
               }
            }
         }

         xhr.send(data);
      })
   },
   async upload(url, blob, name, process, i=0) {
      /** 
       * fileinfo:{
       * path,
       * name,
       * size,
       * }
      */
      return new Promise(async (resolve, reject) => {
         let size = blob.size;
         const chunk_size = 102400;
         let total = Math.ceil(parseFloat(size) / chunk_size);
         //let i = 0;
         let res = null;
         let retry = 0;
         while (i < total) {
            try {
               let form = new FormData();
               form.append("i", i);
               form.append("total", total);
               form.append("name", name);
               form.append("chunk", blob.slice(i * chunk_size, (i + 1) * chunk_size));

               res = await HostX.post(url, form);
               if (res.status == 'OK') {
                  process && process({ total: total, i: i + 1 });
                  i += 1;
               } else {
                  throw (res)
               }

            } catch (e) {
               console.log(e)
               retry += 1;
               if (retry > 10) {
                  resolve({ 'status': 'FAIL', 'errors': e, 'i': i })
                  return
               }
            }
         }
         resolve(res)
      })
   }

}
