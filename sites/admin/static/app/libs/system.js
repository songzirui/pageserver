var System = {
    G: {},
    PREFIX: "a:",
    host: {
        get() {
            return ""
        }
    },
    login: {
        //登录信息
        get: function () {
            return System.config.get("loginid");
        },
        set: function (login_info) {
            System.config.set('loginid', login_info)
        },
    },
    cache: {
        // 缓存
        set: function (key, value) {
            if (value == undefined) {
                value = key;
                key = "";
            }
            System.G[key] = JSON.stringify({ _: value })

        },
        get: function (key) {
            if (key == undefined) {
                key = "";
            }
            try {
                return JSON.parse(System.G[key])._;
            } catch (e) {
                return undefined;
            }
        },
    },
    config: {
        get(name) {
            name = System.PREFIX+name
            try {
                return JSON.parse(localStorage.getItem(name))._;
            } catch (e) {
                return null;
            }
        },
        set(name, value) {
            name = System.PREFIX+name
            localStorage.setItem(name, JSON.stringify({ _: value }));
        },
        remove(name) {
            name = System.PREFIX+name
            localStorage.removeItem(name);
        },

    },
};
