edit={
    onCreate(self, page_id, para){
        document.getElementById(page_id).style.backgroundColor="rgba(0,0,0,0)"
        this.app = new Vue({
            el: `#${page_id}`,
            data: {
                model_name: para.model_name,
                form_type: null, //new|update
                form: {},
                obj: copy(para.obj),
                files: {},
                fields: para.fields,
                waiting: {
                    submit: false,
                },
            },
            created() {
                this.init()
            },
            methods: {
                init(){
                    console.log("edit", copy(para))
                    if(this.obj){
                        for (let field of this.fields) {
                            if(field.choice){
                                field.type = "select"
                            }
                        }
                        this.form_type = 'edit'
                        this.form = this.obj    
                    }else{
                        this.form_type = 'new'
                        let form = {}
                        for (let field of this.fields) {
                            form[field.name] = field.default
                            if(field.choice){
                                field.type = "select"
                            }
                        }
                        this.form = form

                    }
                },
                get_verbose_name(field) {
                    return field.verbose_name || field.name
                },    
                select_file(event, field){
                    var files = event.target.files;
                    for(let f of files){
                        console.log(f)
                        this.files[field.name] = f
                    }
                },
    
                get_form_data() {
                    let data = new FormData()
                    for (let field of this.fields) {
                        let name = field.name;
                        if (field.type == 'auto' && this.form_type == 'new') continue;
                        if (field.nullable == false && this.form[name] === "") {
                            $toast("请填写" + this.get_verbose_name(field))
                            return null
                        }
                        if(this.files[name]!=undefined){
                            data.set(name, this.files[name])
                        }else{
                            console.log(name, this.form[name])
                            data.set(name, this.form[name])
                        }
                    }
    
                    return data
    
                },
                submit() {
                    let form_data = this.get_form_data()
                    if (form_data == null) return;
                    this.waiting.submit = true
                    Host.post(`/admin/api/admin?model=${this.model_name}&action=${this.form_type}`, form_data, data => {
                        console.log(data)
                        this.waiting.submit = false
                        if (data.status == 'OK') {
                            para.callback()
                            main.go_back()
                        } else {
                            $toast(data.errors)
                        }
                    })
                },
                
                delete_obj(){
                    this.waiting.submit = true
                    let pk = null
                    for (let field of this.fields) {
                        if (field.primary_key){
                            pk = field.name
                        };
                    }
    
                    Host.delete(`/admin/api/admin`, {model: this.model_name, pk: this.form[pk]}, data => {
                        this.waiting.submit = false
                        if (data.status == 'OK') {
                            para.callback()
                            main.go_back()
                        } else {
                            $toast(data.errors)
                        }
                    })
                }
            }
        })
    },
    onShow(){
    },
    onStop(){
        this.app.$destroy()
    },
}