backup = {
    onCreate(self, page_id) {
        this.app = new Vue({
            el: `#${page_id}`,
            data: {
                selected_all: false,
                selected: [],
                object_list: [],                
                waiting: {
                    loading: true,
                },
            },
            created() {
                this.getObjectList()
            },
            watch:{
                selected(val, old){
                    if(this.selected.length!=this.object_list.length){
                        this.selected_all = false
                    }else{
                        this.selected_all = true
                    }
                }
            },
            methods: {
                selectAll(){
                    this.selected_all = !this.selected_all
                    if(this.selected_all){
                        for(let o of this.object_list){
                            this.selected.push(o.model_name)
                        }
                    }else{
                        this.selected = []
                    }
                },
                getObjectList() {
                    Host.get("/admin/api/model/", data=>{
                        this.waiting.loading = false
                        if(data.status=="OK"){
                            this.object_list = data.model_list
                            return
                        }
                    })
                },
                exportData(){
                    /*
                    if(!this.selected.length){
                        return
                    }*/
                    let url = window.location.origin+"/admin/api/backup/export/";
                    let form = document.createElement('form');
                    form.id = 'form';
                    form.name = 'form';
                    document.body.appendChild (form);
                    for (let v in this.selected) {
                        let input = document.createElement('input')
                        input.type='hidden'
                        input.name = 'model';
                        input.value = v
                        form.appendChild(input)

                    };
                    form.method = 'post'; 
                    form.action = url; 
                    form.submit();
                    document.body.removeChild(form)
                },
                async importData(){
                    /*
                    if(!this.selected.length){
                        return
                    }*/
                    // 创建form
                    let file = this.$refs.inputFile.files[0]
                    console.log(file)
                    await this.putfile({
                        name: file.name,
                        size: file.size,
                        blob: file,
                        process: 0,
                    })
                },
                async putfile(x){
                    let res = await HostX.upload('/admin/api/upload/block/', x.blob, x.name, (res) => {
                        x.process = (res.i / res.total) * 100
                    })
                    console.log(res)
                    if(res.status != "OK"){
                        return
                    }
                    let protocol = location.protocol=='https:'?'wss:':'ws:'
                    let url = `${protocol}//${location.host}/admin/ws/backup/import`
                    let ws = new WebSocket(url)
                    ws.onopen = ()=>{
                        ws.send(JSON.stringify({fk: res.file_key}))
                    }
                    ws.onmessage = (e)=>{
                        let data = JSON.parse(e.data)
                        console.log(data)
                    }
                    ws.onclose = ()=>{
                        console.log('close')
                    }
                    //let res = await Host.post(this.post_url, { fk: x.file_key })
                },
            }
        })
    },
    onShow(self) {
    },
    onStop(self) {
        this.app.$destroy()
    }
}
