login={
    onCreate(){
        this.app = new Vue({
            el: '#'+page_id,
            data:{
                form:{
                    username: '',
                    password: '',
                },
                now: new Date(),
                success_page: 'index',
                runid: null,
            },
            created(){
                this.check_login()
                this.runid = setInterval(this.update_now, 1000)
            },
            methods:{
                update_now(){
                    this.now = new Date()
                },   
                success(){
                    main.reset_page(this.success_page)
                },
                check_login(){
                    Host.get("/admin/api/login/", function(data){
                        if(data.status=="OK"){
                            this.success()
                        }                            
                    }, this)
                },
                login(){
                    System.login.set(null)
                    Host.post("/admin/api/login/", this.form, function(data){
                        console.log(data)
                        if(data.status=='OK'){
                            System.login.set(data.token)
                            this.success()
                        }else{
                            $toast("用户名或密码错误")
                        }    
                    }, this)
                }
            }
        })
    },
    onShow(){
    },
    onStop(){
        clearInterval(this.app.runid)
        this.app.$destroy()
    },
}