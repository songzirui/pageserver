var APP_CONFIG = {
    pages:{
		login:{},
	    index:{},
	    model:{
			pages:{
				edit:{
					animation: null,
				},
				fields:{
					animation: 'left',
				}
			}
		},
		account:{},
		cache: {
			pages:{
				db:{}
			}
		},
		backup: {},
		my:{},
	},
    settings: {
		animation: 'top',
		lazy: true,
        page_path: 'static/app/',
        onLoaded: function(){
		    this.new_page("login");
        }
    },
}