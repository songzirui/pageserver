index={
    onCreate(self, page_id){

        this.app = new Vue({
            el: '#'+page_id,
            data: {
                profile: {},
                count: {},
            },
            created() {
                this.get_model_list()
                this.get_overview()
            },
            methods: {
                get_overview(){
                    Host.get("/admin/api/overview", data=>{
                        if(data.status == 'OK'){
                            this.count = data.data
                        }else{
                            $toast('data.errors')
                        }
                    })
                },
                config_menu(model_items){
                    menu = new Menu({
                        el: "#menu",
                        items:[
                            {label: '概览', name: 'index',  page:'index',},
                            {label: '数据库', items: model_items},
                            {label: '管理员', name: 'user', page: 'account'},
                            {label: '缓存', name: 'cache', page: 'cache'},
                            {label: '备份', name: 'backup', page: 'backup'},
                            {label: '我的账号', name: 'my',  page:'my'}
                        ],
                        header:{
                            html: "数据中心"
                        },
                        footer:{
                            html:"退出",
                            onclick(){
                                System.login.set(null)
                                menu.show(false)
                                main.reset_page('login')
                            }
                        },
                        onclick(item){
                            let name = item.name
                            let page=item.page
                            main.go_back("index")
                            if(name!="index"){
                                main.new_page(page, item.para)
                            }
                        },
                        created(){
                            if (window.location.hash) {
                                //启动上次页面
                                let name = window.location.hash.substr(1)
                                this.select(name)
                            }                        
                        }
                    })  
                    menu.show(true)  
                                
                },
                get_model_list() {
                    Host.get("/admin/api/model/", data=>{
                        if(data.status!="OK"){
                            return
                        }
                        let model_items = []
                        for(let obj of data.model_list){
                            model_items.push({
                                name: '@'+obj.model_name,
                                label: obj.verbose_name,
                                page: 'model',
                                para: obj,
                            })
                        }
                        this.config_menu(model_items)

                    })
                },
            }
        })
    },
    onShow(){
    },
    onStop(){
        console.log('stop')
        this.app.$destroy()
    },
}