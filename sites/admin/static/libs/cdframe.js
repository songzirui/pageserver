/***
 * cdframe 单页面应用框架
 */
; (function () {
    var STYLE = ".pageX{height:100%; width:100%;background-color:#fff;position: absolute; left:0;top:0; overflow-x:hidden;z-index:-1;}" +
        "@keyframes pageXShowleft{from {left: 100%;}to {left: 0;}} @keyframes pageXBackleft{from {left: 0;}to {left: 100%;}}" +
        "@keyframes pageXShowright{from {left: -100%}to {left: 0;}} @keyframes pageXBackright{from {left: 0;}to {left: -100%;}}" +
        "@keyframes pageXShowtop{from {top: -100%}to {top: 0;}} @keyframes pageXBacktop{from {top: 0;}to {top: -100%;}}"+
        "@keyframes pageXShowbottom{from {top: 100%}to {top: 0;}} @keyframes pageXBackbottom{from {top: 0;}to {top: 100%;}}";

    var SETTINGS = {
        animation: ['left', 'right', 'top', 'bottom', null],
        lazy: [false, true],
    }
    function cfg(config, k, default_value) {
        var v = config[k]
        if (SETTINGS[k].indexOf(v) > -1) {
            return v
        }
        if(default_value!=undefined){
            return default_value
        }
        return SETTINGS[k][0]
    }
    function attr(obj, name, default_value) {
        return (obj[name] !== undefined) ? obj[name] : default_value;
    }

    function copy(val) {
        return JSON.parse(JSON.stringify({ _: val }))._
    }
    function FnQueue(context){
        this.queue = []
        this.lock = false
        this.next = function () {
            while (this.queue.length > 0) {
                var obj = this.queue.shift();
                var fn = obj[0], async = obj[1], para = obj[2];
                this.lock = true;
                try {
                    fn.apply(context, para);
                } catch (e) {
                    console.error(e)
                    this.lock = false;
                }
                if (async) {
                    return;
                }
            }
            this.lock = false;
        },
        this.call = function (fn, para, async) {
            this.queue.push([fn, async, para]);

            if (!this.lock) {
                this.next()
            }
        },
        this.clean = function () {
            //取消剩余队列
            this.queue = [];
        }

    }

    var Http = {
        get: function (url, callback, context) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = "text";
            xhr.setRequestHeader('If-Modified-Since', '0');

            xhr.onload = function () {
                if (this.status == 200) {
                    callback.call(context, this.response);
                }else {
                    console.error(this.response)
                    callback.call(context, "");
                }
            }
            xhr.send();
        },
    }

    var Compiler = {
        js: function (page, page_id) {
            //加载js
            var mod_name = page.path[page.path.length - 1]
            var load_name = page.js.slice(0, mod_name.length)
            if (load_name != mod_name) {
                console.error("加载JS错误 mod_name不匹配 " + mod_name + ':' + load_name)
                return null
            }
            eval(page_id + '_' + page.js); //初始化js
            var app = window[page_id+'_'+mod_name];
            delete window[page_id+'_'+mod_name];
            return app
        }
    }

    function Pages(pages, src, animation, lazy) {
        function config(pages, parents) {
            if (parents == undefined) parents = [];
            var res = {};

            for (var name in pages) {
                var o = pages[name];
                var path = parents.concat(name);
                var page_name = path.join('.')
                var _src = path.join('/')
                res[page_name] = {
                    'path': path,
                    'name': page_name,
                    'load': false,
                    'html': '',
                    'js': '',
                    'css': '',
                    'settings': {
                        'html': attr(o, 'html', src + _src + '/page.html'),
                        'js': attr(o, 'js', src + _src + '/page.js'),
                        'css': attr(o, 'css', src + _src + '/page.css'),
                        'lazy': cfg(o, 'lazy', lazy),
                        'animation': cfg(o, 'animation', animation),
                    },
                }
                if (o.pages) {
                    var _res = config(o.pages, path)
                    for (var p in _res) {
                        res[p] = _res[p];
                    }
                }

            }
            
            return res
        }
        this.src = src
        this.animation = animation
        this.lazy = lazy
        this.pages = config(pages)
        this.get = function (name) {
            return this.pages[name];
        }
    }

    var Dom = {
        create(tag) {
            return document.createElement(tag)
        },
        $(selector) {
            if (selector[0] == "#") {
                return [document.getElementById(selector.substr(1))]
            }
            if (selector[0] == '.') {
                return document.getElementsByClassName(selector.substr(1))
            }
            return document.getElementsByTagName("head")
        },
        init_style: function (el) {
            var css = window.getComputedStyle(el);
            var width = css.width;
            var height = css.height;
            if (width == '0px') {
                el.style.width = '100vw'
            }
            if (height == '0px') {
                el.style.height = '100vh'
            }

            var _style = this.create("style");
            _style.innerHTML = STYLE;
            this.$("head")[0].appendChild(_style);
            el.style.overflow = "hidden"
            el.style.position = "relative"
        },
        add_style: function (id, content) {
            var el = this.create("style");
            el.id = id;
            el.innerHTML = content;
            this.$("head")[0].appendChild(el);
        },
        add_div: function (el, id, html) {
            var div = this.create("div");
            div.className = "pageX"
            div.id = id;
            div.innerHTML = html;
            el.appendChild(div);
            return div;
        },
        remove_el: function (selector) {
            var els = this.$(selector)
            for (var i = 0; i < els.length; i++) {
                var el = els[i];
                //console.log(el, i, els)
                el.parentNode.removeChild(el)
            }
        },
        remove_el_by_class: function (class_name) {
            var els = document.getElementsByClassName(class_name)
            for (var i = 0; i < els.length; i++) {
                var el = els[i];
                //console.log(el, i, els)
                el.parentNode.removeChild(el)
            }
        }
    }
    window.CDFrame = function CDFrame(el, config) {
        this.version = 20210514;
        this.VERSION = '1.2';
        this.el = Dom.$("#"+el)[0];
        Dom.init_style(this.el);
        this.lock = false; //事件锁定
        this.G = {};
        this.history = [];
        this.objs = {};
        this.current = null; //当前页面js对象
        //片段 1.1新增
        this.fragments = attr(config, 'fragments', [])
        this.settings = {
            animation: cfg(config.settings, 'animation'),
            lazy: cfg(config.settings, 'lazy'),
            src: attr(config.settings, 'src', attr(config.settings, 'page_path', 'templates/')),
            onLoaded: attr(config.settings, 'onLoaded', function () { console.log("onLoaded") }),
        };

        this.pages = new Pages(
            config.pages,
            this.settings.src,
            this.settings.animation, 
            this.settings.lazy);
        
        this.$core = {
            http: Http,
        }
        
        this.queue = new FnQueue(this);

        this.getG = function (key) {
            var value = this.G[key];
            if (value) {
                delete this.G[key];
            }
            return value
        }

        this.load_fragment = function (name, url) {
            this.queue.call(function (name, url) {
                console.log('加载片段: ' + name);
                var self = this;
                Http.get(url, function (data) {
                    this.fragments[name] = data;
                    this.queue.next()
                }, this)
            }, [name, url], true)
        }

        this.load_template = function (name) {
            console.log('加载页面: ' + name);
            var page = this.pages.get(name)
            if (page.load) return;
            page.load = 'loading';

            this.queue.call(function (name) {
                var page = this.pages.get(name);
                var self = this;
                Http.get(page.settings.html, function (data) {
                    var reg = /<fragment>(\w+)<\/fragment>/g;
                    if(reg.test(data)){
                        data = data.replace(reg, self.fragments[RegExp.$1])
                    }
                    page.html = data;
                    this.queue.next()
                }, this)
            }, [name], true)

            this.queue.call(function (name) {
                var page = this.pages.get(name);
                Http.get(page.settings.js, function (data) {
                    page.js = data;
                    this.queue.next()
                }, this)
            }, [name], true)

            this.queue.call(function (name) {
                var page = this.pages.get(name);
                Http.get(page.settings.css, function (data) {
                    page.css = data;
                    this.queue.next()
                }, this)
            }, [name], true)

            this.queue.call(function (name) {
                this.pages.get(name).load = true;
            }, [name], false)
        }

        this.destroy_page = function (page_id) {
            //销毁页面
            //console.log('销毁', page_id);
            try {
                var obj = this.objs[page_id];
                delete this.objs[page_id]
                obj.onStop && obj.onStop()
                obj.app = undefined;
                Dom.remove_el("#"+page_id);
                Dom.remove_el("#"+page_id + '_style');

            } catch (e) {

            }
        }

        this.get_page_name=function(name){
            var obj = this.objs[this.history[0]]
            if(name.substr(0, 2)==".."){
                return name.replace("..", obj ? obj.path.slice(0, -2).join('.')+'.' : "")
            }
            if (name.substr(0, 1)=='.') {
                return name.replace(".", obj ? obj.name+'.' : "")
            }
            return name
        }

        this.new_page = function (name, para, animation) {
            
            console.log('打开新页面 ', name);
            name = this.get_page_name(name)

            var page = this.pages.get(name);
            if (!page) {
                console.warn("页面不存在: " + name)
                return
            }

            if (!page.load) {
                this.load_template(name);
                this.queue.call(this.new_page, [name, para, animation], false)
                return
            }
            if (this.lock) return
            this.lock = true
            var page_id = 'page_' + (new Date()).valueOf();
            if (animation === undefined) { animation = page.settings.animation }

            var app = Compiler.js(page, page_id)
            if (app == null) {
                return
            }
            this.history.unshift(page_id);
            var obj = {
                'path': copy(page.path),
                'name': name,
                'page_id': page_id,
                'app': app,
                'animation': animation,
            };
            this.objs[page_id] = obj;
            

            Dom.add_style(page_id + '_style', page.css); //添加css
            Dom.add_div(this.el, page_id, page.html);//添加dom
            this.current = app
            app.onCreate && app.onCreate(app, page_id, para); //运行JS

            var page_dom = Dom.$("#"+page_id)[0];
            page_dom.style.zIndex = this.history.length;

            //动画
            if (animation && this.history.length > 1) {
                var self = this;
                function animationEndFn() {
                    this.removeEventListener("webkitAnimationEnd", animationEndFn);
                    self.lock = false;
                    app.onShow && app.onShow(app);
                }
                page_dom.addEventListener("webkitAnimationEnd", animationEndFn)
                page_dom.style.animation = 'pageXShow' + animation + ' 0.3s';

            } else {
                switch (animation) {
                    case 'top':
                        page_dom.style.top = 0;
                        break
                    case 'right':
                    default:
                        page_dom.style.left = 0;
                }
                Dom.remove_el_by_class("pageX reset-page-cover")
                app.onShow && app.onShow(app, para);
                this.lock = false
            }
            return page_id;

        }

        this.go_back = function (page_tag, animation) {
            //page_tag = page_id or page_name
            if (this.history.length == 1) {
                return
            }
            //console.log('go_back', page_tag)

            var page_id = this.history.shift()
            var obj = this.objs[page_id];
            var page_dom = Dom.$("#"+page_id)[0];
            obj.app.onPause && obj.app.onPause(obj.app)

            if (page_tag != undefined) {
                if (page_id != page_tag || obj.name != page_tag) {
                    while (this.history.length > 1) {
                        var _page_id = this.history[0];
                        var _obj = this.objs[_page_id];
                        if (_page_id === page_tag || _obj.name == page_tag) break;
                        this.history.shift();
                        this.destroy_page(_page_id);
                    }
                }

            }

            if (animation === undefined) { animation = obj.animation }

            this.current = this.objs[this.history[0]].app;

            if (animation) {
                page_dom.addEventListener("webkitAnimationEnd", () => {
                    this.destroy_page(page_id);
                    this.current.onShow && this.current.onShow(this.current);
                })
                page_dom.style.animation = 'pageXBack' + animation + ' 0.3s';

            } else {
                switch (animation) {
                    case 'top':
                        page_dom.style.top = '-100%';
                        break
                    case 'right':
                        page_dom.style.left = '-100%';
                        break
                    default:
                        page_dom.style.left = '100%';
                }
                this.destroy_page(page_id);
                this.current.onShow && this.current.onShow(this.current);
            }
        }
        this.reset_page = function (name, para) {
            //重置页面, 无动画
            console.log("重置", name)
            name = this.get_page_name(name)

            var page_id = this.history[0];
            var obj = this.objs[page_id];
            if (obj.name == name) { return }
            this.history.shift();
            this.destroy_page(page_id);

            //添加遮罩dom
            var div = Dom.create("div");
            div.classList.add("pageX", "reset-page-cover");
            div.style.backgroundColor = "#fff";
            div.style.zIndex = this.history.length;
            this.el.appendChild(div);
            this.new_page(name, para, null);
        }

        this.start_page = function (name, para) {
            //重置页面, 无动画
            while (this.history.length > 0) {
                var page_id = this.history.shift();
                this.destroy_page(page_id);
            }
            this.new_page(name, para, null);
        }

        for (var name in this.fragments) {
            var url =  this.settings.src + this.fragments[name]
            this.load_fragment(name, url);
        }

        for (var name in this.pages.pages) {
            //console.log(name)
            if (!this.pages.get(name).settings.lazy) {
                this.load_template(name);
            }
        }

        this.queue.call(function () {
            this.settings.onLoaded.call(this);
        }, null, false)

    }
})();
