/***
 * cdframe 单页面应用框架
 */


var HttpX = {
    _get: function (url, callback, context) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = "text";
        xhr.setRequestHeader('If-Modified-Since', '0');

        xhr.onload = function () {
            if (this.status == 200) {
                var key = "page@"+url;
                System.config.set(key, this.response);
                callback.call(context, this.response);
            } else {
                callback.call(context, this.response);
            }
        }
        xhr.send();
    },
    get: function (url, callback, context) {
        var key = "page@"+url;
        var code = System.config.get(key);
        if(code == undefined){
            HttpX._get(url, callback, context)
        }else{
            callback.call(context, code);
        }
    },
}
