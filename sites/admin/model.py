from pageserver.db.models import *
from pageserver.utils.crypto import check_password, make_password
from pageserver.utils.time import get_timestamp, bj_now
from pageserver.cache import cache


def get_obj(model_class, obj_id, clean=False):
    _key = "{}:{}".format(model_class.__name__, obj_id)
    if clean:
        cache.delete(_key)
        return

    _data = cache.get(_key)
    if _data is None:
        obj = model_class.query(id=obj_id).one()
        if obj is None:
            return None
        _data = obj.to_dict()
        cache.set(_key, _data, 600)

    obj = model_class(**_data)
    obj.instance = _data[model_class._meta['primary_key']]
    return obj


class AdminAccount(Model):
    id = AutoField(primary_key=True)
    username = CharField(max_length=32, unique=True)
    keygen = CharField(max_length=256, default=None)
    password = CharField(max_length=128, nullable=False)
    nickname = CharField(max_length=16, default="")
    is_super = IntegerField(default=0)
    is_active = IntegerField(default=1)
    create_time = TimestampField(default=get_timestamp)
    token = IntegerField(nullable=True, verbose_name="标记")  # 判断登陆信息是否有效

    meta = {
        'db_table': 'admin_account',
        'verbose_name': '运维帐号',
        'use_db': 'admin',
    }

    def __str__(self):
        return "{}.{}".format(self.username, self.password)

    def check_password(self, raw_password):
        return check_password(raw_password, self.password)

    def set_password(self, raw_password, keygen=None):
        # 每次保存密码重置token
        self.password = make_password(raw_password)
        self.keygen = keygen
        self.token = get_timestamp()

    def pre_save(self):
        if not self.create_time:
            self.create_time = get_timestamp()

        if not self.password.startswith('pbkdf2_sha256$'):
            self.set_password(self.password)

    def post_save(self):
        get_obj(self.__class__, self.id, clean=True)

    def check_keygen(self, raw_key):
        print(raw_key)
        now = bj_now()
        return True


