import os
BASE_DIR = os.getcwd()
TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
STATIC_DIR = os.path.join(BASE_DIR, 'static')
TEMP_DIR = os.path.join(BASE_DIR, 'tmp')

DEBUG = True

URLS = "urls"

MIDDLEWARE = []

SECRET_KEY = "CC,$G!&Nj?pRjiL}WkNq#?!D^ikj.kASOED1*~PNjn,s%8u->hvZ2bfJEe-+(&n1"

DATABASE = {
    "default": {
        "engine": "sqlite3",
        "name": os.path.join(BASE_DIR, "db.sqlite3")
    },
    #"admin": {
    #    "engine": "sqlite3",
    #    "name": os.path.join(BASE_DIR, "db.sqlite3")
    #},
    #"migrate": {
    #    "engine": "sqlite3",
    #    "name": os.path.join(BASE_DIR, "db.sqlite3")
    #}
}

CACHE = {
    "default": {
        "engine": "memory"
    }
}

LAYER = {
    "default": {
        "engine": "memory"
    }
}

ADMIN = {
    "config": "config",
}